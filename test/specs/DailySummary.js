var firebase = require("firebase");
var config = {
    apiKey: "AIzaSyCGyKk7fHZ4qZuR1dbwo8hUqM3tFiRKT3E",
    authDomain: "investment--Watch.firebaseapp.com",
    databaseURL: "https://investment--Watch.firebaseio.com/",
    storageBucket: "bucket.appspot.com"
};

firebase.initializeApp(config);


// Get a reference to the database service
var database = firebase.database();


describe('Get Market Summary', () => {
    before('Go To Page', async () => {
        await browser.url('https://www.jamstockex.com/market-data/combined-market/summary')
    })
    it('Additional Information', async () => {

        let dateSelector = await browser.$(`h2`)
        let date = await dateSelector.getText()
        let newDate = date.split('Market for ')[1]
        let AdvancingStocksCountSelector = await browser.$$('table:nth-child(13) tbody tr')
        let AdvancingStocksCount = AdvancingStocksCountSelector.length

        let DecliningStocksCountSelector = await browser.$$('table:nth-child(17) tbody tr')
        let DecliningStocksCount = DecliningStocksCountSelector.length


        let TradingFirmStocksSelector = await browser.$$('table:nth-child(21) tbody tr')
        let TradingFirmStocks = TradingFirmStocksSelector.length

        let data = {
            newDate,
            AdvancingStocksCount,
            DecliningStocksCount,
            TradingFirmStocks
        }

        database.app.database().ref('Information').set(
            data
        )
        console.log('Information Saved!')
    })
    it('Advancing Stocks', async () => {
        let stocks = await browser.$('table:nth-child(13) tbody')
        let amount = await stocks.$$('tr')
        let array = []
        for (let i = 0; i < amount.length; i++) {
            let id = i + 1;
            let nameSelector = await browser.$(`table:nth-child(13) tbody tr:nth-child(${i + 1}) td:nth-child(1)`)
            let name = await nameSelector.getText()

            let volumeSelector = await browser.$(`table:nth-child(13) tbody tr:nth-child(${i + 1}) td:nth-child(2)`)
            let volume = await volumeSelector.getText()

            let closingPriceSelector = await browser.$(`table:nth-child(13) tbody tr:nth-child(${i + 1}) td:nth-child(3)`)
            let closingPrice = await closingPriceSelector.getText()

            let priceChangeSelector = await browser.$(`table:nth-child(13) tbody tr:nth-child(${i + 1}) td:nth-child(4)`)
            let priceChange = await priceChangeSelector.getText()

            let changeSelector = await browser.$(`table:nth-child(13) tbody tr:nth-child(${i + 1}) td:nth-child(5)`)
            let change = await changeSelector.getText()

            let data = {
                id,
                name,
                volume,
                closingPrice,
                priceChange,
                change
            }
            array[i] = data

        }
        database.app.database().ref('AdvancingStocks').set(
            array
        )
        console.log('Advancing Stocks Saved!')
    })
    it('Declining Stocks', async () => {
        let stocks = await browser.$('table:nth-child(17) tbody')

        let amount = await stocks.$$('tr')
        let array = []
        for (let i = 0; i < amount.length; i++) {
            let id = i + 1;

            let nameSelector = await browser.$(`table:nth-child(17) tbody tr:nth-child(${i + 1}) td:nth-child(1)`)
            let name = await nameSelector.getText()

            let volumeSelector = await browser.$(`table:nth-child(17) tbody tr:nth-child(${i + 1}) td:nth-child(2)`)
            let volume = await volumeSelector.getText()

            let closingPriceSelector = await browser.$(`table:nth-child(17) tbody tr:nth-child(${i + 1}) td:nth-child(3)`)
            let closingPrice = await closingPriceSelector.getText()

            let priceChangeSelector = await browser.$(`table:nth-child(17) tbody tr:nth-child(${i + 1}) td:nth-child(4)`)
            let priceChange = await priceChangeSelector.getText()

            let changeSelector = await browser.$(`table:nth-child(17) tbody tr:nth-child(${i + 1}) td:nth-child(5)`)
            let change = await changeSelector.getText()

            let data = {
                id,
                name,
                volume,
                closingPrice,
                priceChange,
                change
            }

            array[i] = data

        }
        database.app.database().ref('DecliningStocks').set(
            array
        )
        console.log('Declining Stocks Saved!')
    })
    it('TradingFirm Stocks', async () => {
        let stocks = await browser.$('table:nth-child(21) tbody')

        let amount = await stocks.$$('tr')
        let array = []
        for (let i = 0; i < amount.length; i++) {
            let id = i + 1;

            let nameSelector = await browser.$(`table:nth-child(21) tbody tr:nth-child(${i + 1}) td:nth-child(1)`)
            let name = await nameSelector.getText()

            let volumeSelector = await browser.$(`table:nth-child(21) tbody tr:nth-child(${i + 1}) td:nth-child(2)`)
            let volume = await volumeSelector.getText()

            let closingPriceSelector = await browser.$(`table:nth-child(21) tbody tr:nth-child(${i + 1}) td:nth-child(3)`)
            let closingPrice = await closingPriceSelector.getText()

            let data = {
                id,
                name,
                volume,
                closingPrice,

            }
            array[i] = data

        }
        database.app.database().ref('TradingFirmStocks').set(
            array
        )
        console.log('Trading Firm Stocks Saved!')
    })
    it('Ordinary Shares', async () => {
        await browser.url('https://www.jamstockex.com/market-data/combined-market/quote/')

        let stocks = await browser.$$('#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr')
        let array = []
        for (let i = 0; i < stocks.length; i++) {

            let id = i + 1;

            let performanceSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td.text-left > img`)
            let nameSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td.text-left > a`)
            let lastTradedPriceSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2)`)
            let closePriceSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(3)`)
            let priceChangeSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(4)`)
            let closingBidSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(5)`)
            let closingAskSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(6)`)
            let volumeSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(7)`)
            let todaysHighSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(8)`)
            let todaysLowSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(9)`)
            let weekHigh52Selector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(10)`)
            let weekLow52Selector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(11)`)
            let prevYrDivSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(12)`)
            let currentDivSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(6) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(13)`)

            let code = await nameSelector.getText()
            let name = await nameSelector.getAttribute('data-original-title')
            let lastTradedPrice = await lastTradedPriceSelector.getText()
            let priceChange = await priceChangeSelector.getText()
            let closePrice = await closePriceSelector.getText()
            let closingBid = await closingBidSelector.getText()
            let closingAsk = await closingAskSelector.getText()
            let volume = await volumeSelector.getText()
            let todaysHigh = await todaysHighSelector.getText()
            let todaysLow = await todaysLowSelector.getText()
            let weekLow52 = await weekLow52Selector.getText()
            let weekHigh52 = await weekHigh52Selector.getText()
            let prevYrDiv = await prevYrDivSelector.getText()
            let currentDiv = await currentDivSelector.getText()
            let performanceCheck = await performanceSelector.getAttribute('src')

            let status = performanceCheck.split('img/')[1].split('.png')[0]

            let data = {
                id,
                code,
                name,
                status,
                lastTradedPrice,
                closePrice,
                priceChange,
                closingBid,
                closingAsk,
                volume,
                todaysHigh,
                todaysLow,
                weekLow52,
                weekHigh52,
                prevYrDiv,
                currentDiv
            }
            array[i] = data
        }
        database.app.database().ref('OrdinaryShares').set(
            array
        )
        console.log('Ordinary Shares Saved!')
    })
    it('Indices Data', async () => {
        await browser.url('https://www.jamstockex.com/market-data/combined-market/quote/')
        let stocks = await browser.$$('#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr')
        let array = []
        for (let i = 0; i < stocks.length; i++) {

            let id = i + 1;

            let indexNameSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(1) > a`)
            let index = await indexNameSelector.getText()

            let valueSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(2)`)
            let value = await valueSelector.getText()

            let volumeSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(3)`)
            let volume = await volumeSelector.getText()

            let changeSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(4)`)
            let change = await changeSelector.getText()

            let percentageChangeSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(5)`)

            let percentageChange = await percentageChangeSelector.getText()

            let performanceSelector = await browser.$(`#content > div:nth-child(2) > div > div > div > div > div > div:nth-child(4) > div.table-responsive > table > tbody > tr:nth-child(${i + 1}) > td:nth-child(1) > img`)

            let performanceCheck = await performanceSelector.getAttribute('src')

            let status = performanceCheck.split('img/')[1].split('.png')[0]

            let data = {
                id,
                index,
                value,
                volume,
                change,
                percentageChange,
                status
            }
            array[i] = data
        }
        database.app.database().ref('Indices').set(
            array
        )
        console.log('Indices Stocks Saved!')
    })
})


