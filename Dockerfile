FROM atools/chrome-headless:java8-node12-latest

WORKDIR /test
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .
RUN npm test